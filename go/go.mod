module github.com/jcowgar/acme-utils

go 1.18

require 9fans.net/go v0.0.4

require gopkg.in/yaml.v2 v2.4.0
